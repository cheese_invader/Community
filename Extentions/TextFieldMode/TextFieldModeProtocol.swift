//
//  TextFieldModeProtocol.swift
//  Community
//
//  Created by Marty on 13/09/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

protocol TextFieldModeProtocol: NSObjectProtocol {
    func setVisualMode(_ mode: TextFieldVisualMode)
}

