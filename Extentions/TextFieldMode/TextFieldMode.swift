//
//  TextFieldMode.swift
//  Community
//
//  Created by Marty on 13/09/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation
import UIKit

private let dangerColor = UIColor.red.cgColor

extension UITextField: TextFieldModeProtocol {
    func setVisualMode(_ mode: TextFieldVisualMode) {
        
        switch mode {
        case .danger:
            self.layer.borderColor  = dangerColor
            self.layer.borderWidth  = 1
            self.layer.cornerRadius = 5
        case .normal:
            self.layer.borderColor  = nil
            self.layer.borderWidth  = 0
            self.layer.cornerRadius = 0
        }
        
    }
}
