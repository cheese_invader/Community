//
//  NetworkWorker.swift
//  Community
//
//  Created by Marty on 10/09/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

// MARK: - Some kind of server -
// Simulation of usage dynamics and error generation

import UIKit

class NetworkWorker {
    // to remember new elements in different modult
    static var shared = NetworkWorker()
    
    private var isBuisy        = false
    private var people         = [Person]()
    
    // never mind that f**king s*it
    private var successCounter          = 0
    private var successUploadingCounter = 0
    private var numberOfPeople          = 0
    private var currentID               = 8
    
    private var slowQueue = DispatchQueue(label: "com.marty.networkQueue")
    
    private init() {
        // My warehouse. Dangerous! Run away!
        var somePerson = Person()
        somePerson.id        = 1
        somePerson.biography = """
                               Marty was born in Hill Valley, California to a family of Irish descent. Little is known about Marty's life prior to the first Back to the Future film, except for the fact that he set fire to the living-room rug when he was 8 years old (which is revealed via a statement of Marty's to his future parents). He met his friend Dr. Emmett "Doc" Brown when he was around fourteen after hearing that Brown was a dangerous lunatic. Marty, being the “red-blooded American teenager” he was, wanted to go see what it was all about for himself. He found Doc’s lab and was fascinated by all his inventions. When Doc caught him, he was glad to have someone who liked his work and their friendship started there.
                               """
        somePerson.firstName = "Marty"
        somePerson.lastName  = "McFly"
        somePerson.city      = "San Francisco"
        somePerson.gender    = .male
        somePerson.photo     = UIImage(named: "Marty")
        somePerson.birthday  = Date()
        people.append(somePerson)
        
        somePerson = Person()
        somePerson.id        = 2
        somePerson.biography = """
                               James Alan Hetfield (born August 3, 1963) is an American musician, singer, and songwriter known for being the co-founder, lead vocalist, rhythm guitarist, and main songwriter for the American heavy metal band Metallica. Hetfield is mainly known for his intricate rhythm playing, but occasionally performs lead guitar duties and solos, both live and in the studio. Hetfield co-founded Metallica in October 1981 after answering a classified advertisement by drummer Lars Ulrich in the Los Angeles newspaper The Recycler. Metallica has won nine Grammy Awards and released ten studio albums, three live albums, four extended plays and 24 singles.
                               """
        somePerson.firstName = "James"
        somePerson.lastName  = "Hetrield"
        somePerson.city      = "Los Angeles"
        somePerson.gender    = .male
        somePerson.photo     = UIImage(named: "James")
        somePerson.birthday  = Date()
        people.append(somePerson)
        
        somePerson = Person()
        somePerson.id        = 3
        somePerson.biography = """
                               Hammett was born on November 18, 1962 in San Francisco, and raised in the town of El Sobrante, California. He is the son of Teofila "Chefela" (née Oyao) and Dennis L. Hammett (a Merchant Marine). His mother is of Filipino descent and his father was of English, German, Scottish and Irish ancestry. He attended De Anza High School in Richmond, California. While attending De Anza High School, he met Les Claypool of Primus, and they remain close friends.
                               """
        somePerson.firstName = "Kirk"
        somePerson.lastName  = "Hammet"
        somePerson.city      = "New York"
        somePerson.gender    = .male
        somePerson.photo     = UIImage(named: "Kirk")
        somePerson.birthday  = Date()
        people.append(somePerson)
        
        somePerson = Person()
        somePerson.id        = 4
        somePerson.biography = """
                               Ulrich was born into an upper-middle-class family in Gentofte, Denmark; the son of Lone (née Sylvester-Hvid) and professional tennis player Torben Ulrich. His paternal grandfather was professional tennis player Einer Ulrich. His paternal grandmother, Ulla Meyer, was from a Jewish family; as a result, Ulrich's grandfather was persecuted by the Nazis during World War II. Saxophonist Dexter Gordon was Ulrich's godfather, and he is a childhood friend of musician Neneh Cherry.
                               """
        somePerson.firstName = "Lars"
        somePerson.lastName  = "Ulrich"
        somePerson.city      = "Danish"
        somePerson.gender    = .male
        somePerson.photo     = UIImage(named: "Lars")
        somePerson.birthday  = Date()
        people.append(somePerson)
        
        somePerson = Person()
        somePerson.id        = 7
        somePerson.biography = """
                                Although mainly known as Gandalf, the character has a number of names in Tolkien's writings. Gandalf himself says, "Many are my names in many countries. Mithrandir among the Elves, Tharkûn to the Dwarves, Olórin I was in my youth in the West that is forgotten, in the South Incánus, in the North Gandalf; to the East I go not."
                               """
        somePerson.firstName = "Gandalf"
        somePerson.lastName  = "the White"
        somePerson.city      = "Washington"
        somePerson.gender    = .male
        somePerson.photo     = UIImage(named: "Gandalf")
        people.append(somePerson)
    }
    
    // Do not touch it. It works badly. How do I need to emulate
    @discardableResult func loadIfNotBuisy(completionSuccess: ( ([Person]) -> Void )?, completionFailure: ( (() -> Void) )?) -> Bool {
        if isBuisy {
            return false
        }
        
        isBuisy = true
        
        slowQueue.asyncAfter(deadline: .now() + 2, execute: { [weak self] in
            guard let strongSelf = self, strongSelf.isBuisy else {
                return
            }
            
            strongSelf.successCounter += 1
            strongSelf.successCounter %= 3
            
            if strongSelf.successCounter == 0 {
                completionFailure?()
            } else {
                let subPeople = strongSelf.people[0...strongSelf.numberOfPeople]
                completionSuccess?(Array(subPeople))
                strongSelf.numberOfPeople += 1
                strongSelf.numberOfPeople %= strongSelf.people.count
            }
            
            strongSelf.isBuisy = false
        })
        
        return true
    }
    
    func uploadPerson(_ persone: Person, completionSuccess: ( (Person) -> Void )?, completionFailure: ( () -> Void )?) {
        slowQueue.asyncAfter(deadline: .now() + 2, execute: { [weak self] in
            guard let strongSelf = self else {
                return
            }
            
            strongSelf.successUploadingCounter += 1
            strongSelf.successUploadingCounter %= 2
            
            if strongSelf.successUploadingCounter == 0 {
                completionFailure?()
            } else {
                persone.id = strongSelf.currentID
                strongSelf.currentID += 1
                strongSelf.people.insert(persone, at: 0)
                completionSuccess?(persone)
            }
        })
    }
}
