//
//  LifeRouter.swift
//  Community
//
//  Created by Marty on 12/09/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import UIKit

private let storyboardName = "Life"
private let storyboardID   = "LifeStoryboard"

class LifeRouter: NSObject {
    private weak var view: LifeViewControllerProtocol?
    private weak var navigationController: NavigationViewController?
    
    init(view: LifeViewControllerProtocol, navigationController: NavigationViewController) {
        self.navigationController = navigationController
        self.view                 = view
    }
}

extension LifeRouter: LifeRouterProtocol {
    static func assembleModule(embeddedIn navigationController: NavigationViewController) -> LifeViewController {
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let lifeVC     = storyboard.instantiateViewController(withIdentifier: storyboardID) as! LifeViewController
        
        let router = LifeRouter(view: lifeVC, navigationController: navigationController)
        
        lifeVC.setup(router: router)
        
        return lifeVC
    }
}
