//
//  LifeRouterProtocol.swift
//  Community
//
//  Created by Marty on 12/09/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import UIKit

protocol LifeRouterProtocol: NSObjectProtocol {
    static func assembleModule(embeddedIn navigationController: NavigationViewController) -> LifeViewController
}
