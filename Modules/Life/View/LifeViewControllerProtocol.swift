//
//  LifeViewControllerProtocol.swift
//  Community
//
//  Created by Marty on 12/09/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import UIKit

protocol LifeViewControllerProtocol: NSObjectProtocol {
    var isSetupped: Bool {get}
    func setup(router: LifeRouterProtocol)
    
    func setPerson(_ person: Person)
}
