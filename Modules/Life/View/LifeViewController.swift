//
//  LifeViewController.swift
//  Community
//
//  Created by Marty on 12/09/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import UIKit

class LifeViewController: UIViewController {
    private var router: LifeRouterProtocol?
    private var setupped = false
    
    private var person = Person()
    
    private func checkIntegrity() {
        assert(self.isSetupped, "Life view controller should be setupped")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        checkIntegrity()
        
        setFields()
    }
    
    @IBOutlet weak var titleNavigationItem: UINavigationItem!
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var photoDescriptionLabel: UILabel!
    @IBOutlet weak var biographyLabel: UILabel!
    
    private func setFields() {
        photoDescriptionLabel.text = person.firstName + " " + person.lastName + " from " + person.city
        biographyLabel.text        = person.biography
        titleNavigationItem.title  = person.firstName
        
        if let avatar = person.photo {
            photo.image = avatar
        } else {
            let imageName = person.gender == .male ? "MaleAvatar" : "FemaleAvatar"
            photo.image   = UIImage(named: imageName)
        }
    }
}

extension LifeViewController: LifeViewControllerProtocol {
    var isSetupped: Bool {
        return setupped
    }
    
    func setup(router: LifeRouterProtocol) {
        self.router = router
        setupped = true
    }
    
    func setPerson(_ person: Person) {
        self.person = person
    }
}
