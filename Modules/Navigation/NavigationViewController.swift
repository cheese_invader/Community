//
//  NavigationViewController.swift
//  Community
//
//  Created by Marty on 07/09/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import UIKit

class NavigationViewController: UINavigationController {
    override func viewDidLoad() {
        super.viewDidLoad()

        let rootVC = FamilyRouter.assembleModule(embeddedIn: self)
        self.pushViewController(rootVC, animated: false)
    }
}
