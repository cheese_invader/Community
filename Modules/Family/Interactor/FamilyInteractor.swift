//
//  FamilyInteractor.swift
//  Community
//
//  Created by Marty on 09/09/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import UIKit
import CoreData

class FamilyInteractor: NSObject {
    private weak var presenter: FamilyPresenterProtocol?
    
    private let networkWorker = NetworkWorker.shared
    
    init(presenter: FamilyPresenterProtocol) {
        self.presenter = presenter
    }
}

extension FamilyInteractor: FamilyInteractorProtocol {
    func syncPeopleWithServer() {
        networkWorker.loadIfNotBuisy(completionSuccess: { [weak self] (people) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.presenter?.syncWithServerSucceed(people)
        }) { [weak self] in
            guard let strongSelf = self else {
                return
            }
            strongSelf.presenter?.syncWithServerFailured()
        }
    }
    
    // main Q
    func fetchPeopleFromDataBase() -> [Person] {
        let request: NSFetchRequest<CDPerson> = CDPerson.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "identifier", ascending: true)
        request.sortDescriptors = [sortDescriptor]
        
        let context = CoreDataStackManager.shared.viewContext
        let allCDPeople = (try? context.fetch(request)) ?? []
        
        var allPeople = [Person]()
        
        for personCD in allCDPeople {
            let person = Person.makeFromCDEntity(personCD)
            allPeople.append(person)
        }
        
        return allPeople
    }
    
    // background. Be careful
    func syncPeopleWithDataBase(_ people: [Person]) {
        CoreDataStackManager.shared.persistentContainer.performBackgroundTask { (context) in
            var newPeopleIDs = Set<Int64>()
            var oldPeopleIDs = Set<Int64>()
            
            let request: NSFetchRequest<CDPerson> = CDPerson.fetchRequest()
            let sortDescriptor = NSSortDescriptor(key: "identifier", ascending: true)
            request.sortDescriptors = [sortDescriptor]
            let oldPeople = (try? context.fetch(request)) ?? []
            
            for person in people {
                newPeopleIDs.insert(Int64(person.id))
            }
            
            for oldPerson in oldPeople {
                oldPeopleIDs.insert(oldPerson.identifier)
            }
            
            let toDeleteIDs = oldPeopleIDs.subtracting(newPeopleIDs)
            let toInsertIDs = newPeopleIDs.subtracting(oldPeopleIDs)
            
            for person in oldPeople where toDeleteIDs.contains(person.identifier) {
                context.delete(person)
            }
            
            for person in people where toInsertIDs.contains(Int64(person.id)) {
                let newPerson = CDPerson(context: context)
                newPerson.composeWithEntity(person)
            }
            
            try? context.save()
        }
    }
}
