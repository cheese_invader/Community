//
//  FamilyInteractorProtocol.swift
//  Community
//
//  Created by Marty on 09/09/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

protocol FamilyInteractorProtocol: NSObjectProtocol {
    func syncPeopleWithServer()
    
    // main Q
    func fetchPeopleFromDataBase() -> [Person]
    
    // background. Be careful
    func syncPeopleWithDataBase(_ people: [Person])
}
