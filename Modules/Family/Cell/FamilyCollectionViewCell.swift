//
//  FamilyCollectionViewCell.swift
//  Community
//
//  Created by Marty on 09/09/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import UIKit

class FamilyCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var city: UILabel!
    
    func assembleFromPerson(_ person: Person) {
        if let photo = person.photo {
            self.image.image = photo
        } else {
            let imageName = person.gender == .male ? "MaleAvatar" : "FemaleAvatar"
            self.image.image = UIImage(named: imageName)
        }
        self.name.text   = person.firstName + " " + person.lastName
        self.city.text   = person.city
    }
}
