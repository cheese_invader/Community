//
//  FamilyCollectionViewController.swift
//  Community
//
//  Created by Marty on 09/09/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import UIKit

private let reuseIdentifier = "FamilyCollectionViewCell"
private let cellInset : CGFloat = 15

class FamilyCollectionViewController: UICollectionViewController {
    private var presenter: FamilyPresenterProtocol?
    private var setupped = false
    
    private var people = [Person]()
    private var loaded = false
    
    // MARK: - actions -
    @IBAction func addPersonButtonWasAdded(_ sender: UIBarButtonItem) {
        presenter?.showBirthModule()
    }
    
    // MARK: - Prepearing for showing -
    private func checkIntegrity() {
        assert(self.isSetupped, "Family collection view controller should be setupped")
        assert(presenter?.isSetupped ?? false, "Family presenter should be setupped")
    }
    
    private func registerNib() {
        let nibCell = UINib(nibName: reuseIdentifier, bundle: nil)
        collectionView?.register(nibCell, forCellWithReuseIdentifier: reuseIdentifier)
    }
    
    private func setupRefresher() {
        let refresher = UIRefreshControl()
        refresher.addTarget(self, action: #selector(reload), for: .valueChanged)
        collectionView?.refreshControl = refresher
    }

    @objc private func reload() {
        presenter?.refresh()
    }

    // MARK: - Collection VC stuff -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        checkIntegrity()
        registerNib()
        setupRefresher()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter?.viewWillAppear(request: !loaded)
        loaded = true
    }

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return people.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! FamilyCollectionViewCell
        cell.assembleFromPerson(people[indexPath.row])
    
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter?.didSelectPerson(people[indexPath.row])
    }
}

// MARK: - UICollectionViewDelegateFlowLayout -
extension FamilyCollectionViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(CGFloat(cellInset), CGFloat(cellInset),
                                CGFloat(cellInset), CGFloat(cellInset))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var cellWidth : CGFloat!
        var cellHeight: CGFloat!
        
        if UIScreen.main.bounds.width > 500 {
            cellWidth = UIScreen.main.bounds.width / 4 - cellInset * 1.25
            cellHeight = cellWidth * 1.3
        } else {
            cellWidth = UIScreen.main.bounds.width / 2 - cellInset * 1.5
            cellHeight = cellWidth * 1.35
        }
        return CGSize(width: cellWidth, height: cellHeight)
    }
}

extension FamilyCollectionViewController: FamilyCollectionViewControllerProtocol {
    var isSetupped: Bool {
        return setupped
    }
    
    func setup(presenter: FamilyPresenterProtocol) {
        self.presenter = presenter
        self.setupped  = true
    }
    
    func setPeople(_ people: [Person]) {
        self.people = people
        
        DispatchQueue.main.async { [weak self] in
            guard let strongSelf = self else {
                return
            }
            
            strongSelf.collectionView?.refreshControl?.endRefreshing()
            strongSelf.collectionView?.reloadData()
        }
    }
    
    func stopRefreshing() {
        DispatchQueue.main.async { [weak self] in
            guard let strongSelf = self else {
                return
            }
            
            strongSelf.collectionView?.refreshControl?.endRefreshing()
        }
    }
}
