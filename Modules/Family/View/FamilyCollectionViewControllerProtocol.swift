//
//  FamilyCollectionViewControllerProtocol.swift
//  Community
//
//  Created by Marty on 09/09/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

protocol FamilyCollectionViewControllerProtocol: NSObjectProtocol {
    var isSetupped: Bool {get}
    func setup(presenter: FamilyPresenterProtocol)
    func setPeople(_ people: [Person])
    func stopRefreshing()
}
