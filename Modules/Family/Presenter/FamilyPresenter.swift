//
//  FamilyPresenter.swift
//  Community
//
//  Created by Marty on 09/09/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import UIKit

class FamilyPresenter: NSObject {
    private weak var view : FamilyCollectionViewControllerProtocol?
    private var router    : FamilyRouterProtocol?
    private var interactor: FamilyInteractorProtocol?
    private var setupped  = false
}

extension FamilyPresenter: FamilyPresenterProtocol {
    var isSetupped: Bool {
        return setupped
    }
    
    func setup(view: FamilyCollectionViewControllerProtocol, router: FamilyRouterProtocol, interactor: FamilyInteractorProtocol) {
        guard !setupped else {
            return
        }
        
        self.view       = view
        self.router     = router
        self.interactor = interactor
        self.setupped   = true
    }
    
    func showBirthModule() {
        router?.showBirthModule()
    }
    
    func refresh() {
        interactor?.syncPeopleWithServer()
    }
    
    func viewWillAppear(request: Bool) {
        if let peopleFromDataBase = interactor?.fetchPeopleFromDataBase() {
            view?.setPeople(peopleFromDataBase)
        }
        if request {
            interactor?.syncPeopleWithServer()
        }
    }
    
    func didSelectPerson(_ person: Person) {
        router?.showLifeModuleWithPerson(person)
    }
    
    func syncWithServerSucceed(_ people: [Person]) {
        view?.setPeople(people)
        interactor?.syncPeopleWithDataBase(people)
    }
    
    func syncWithServerFailured() {
        view?.stopRefreshing()
    }
}
