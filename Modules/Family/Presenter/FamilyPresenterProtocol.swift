//
//  FamilyPresenterProtocol.swift
//  Community
//
//  Created by Marty on 09/09/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import UIKit

protocol FamilyPresenterProtocol: NSObjectProtocol {
    var isSetupped: Bool {get}
    func setup(view: FamilyCollectionViewControllerProtocol, router: FamilyRouterProtocol, interactor: FamilyInteractorProtocol)
    
    func showBirthModule()
    
    func refresh()
    func viewWillAppear(request: Bool)
    func didSelectPerson(_ person: Person)
    
    func syncWithServerSucceed(_ people: [Person])
    func syncWithServerFailured()
}
