//
//  FamilyRouter.swift
//  Community
//
//  Created by Marty on 09/09/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import UIKit

private let storyboardName = "Family"
private let storyboardID   = "FamilyStoryboard"

class FamilyRouter: NSObject {
    private weak var presenter: FamilyPresenterProtocol?
    private weak var navigationController: NavigationViewController?
    
    init(presenter: FamilyPresenterProtocol, navigationController: NavigationViewController) {
        self.navigationController = navigationController
        self.presenter            = presenter
    }
}

extension FamilyRouter: FamilyRouterProtocol {
    static func assembleModule(embeddedIn navigationController: NavigationViewController) -> FamilyCollectionViewController {
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let familyVC   = storyboard.instantiateViewController(withIdentifier: storyboardID) as! FamilyCollectionViewController
        
        let presenter  = FamilyPresenter()
        let interactor = FamilyInteractor(presenter: presenter)
        let router     = FamilyRouter(presenter: presenter, navigationController: navigationController)
        
        presenter.setup(view: familyVC, router: router, interactor: interactor)
        familyVC .setup(presenter: presenter)
        
        return familyVC
    }
    
    func showBirthModule() {
        guard let navigationController = navigationController else {
            return
        }
        let birthVC = BirthRouter.assembleModule(embeddedIn: navigationController)
        navigationController.pushViewController(birthVC, animated: true)
    }
    
    func showLifeModuleWithPerson(_ person: Person) {
        guard let navigationController = navigationController else {
            return
        }
        let lifeVC = LifeRouter.assembleModule(embeddedIn: navigationController)
        lifeVC.setPerson(person)
        navigationController.pushViewController(lifeVC, animated: true)
    }
}
