//
//  FamilyRouterProtocol.swift
//  Community
//
//  Created by Marty on 09/09/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import UIKit

protocol FamilyRouterProtocol: NSObjectProtocol {
    static func assembleModule(embeddedIn navigationController: NavigationViewController) -> FamilyCollectionViewController
    func showBirthModule()
    func showLifeModuleWithPerson(_ person: Person)
}
