//
//  BirthViewControllerProtocol.swift
//  Community
//
//  Created by Marty on 07/09/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

protocol BirthViewControllerProtocol: NSObjectProtocol {
    var isSetupped: Bool {get}
    func setup(presenter: BirthPresenterProtocol)
    
    func resetDanger()
    
    func makeFocusOnField(_ index: Int, visualMode: TextFieldVisualMode)
    func dismissKeyboard()
    
    func setSaveButtonStatus(enabled: Bool)
    func setPhotoForGender(_ gender: Gender)
    
    func setCityIfEmpty(_ city: String)
    
    func showAlert(title: String, message: String)
    func showLocationAskingAlert()
}
