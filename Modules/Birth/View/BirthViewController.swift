//
//  BirthViewController.swift
//  Community
//
//  Created by Marty on 07/09/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import UIKit

private let maximumAge = 100
private let femaleAvatarName = "FemaleAvatar"
private let   maleAvatarName = "MaleAvatar"
private let openSettionsAlertButton = "Open Settings"
private let        gotItAlertButton = "Got it"
private let askLocationAlertTitle   = "Location was denied"
private let askLocationAlertMessage = "If you enable the location in the settings we can define your city"

class BirthViewController: UIViewController {
    private var presenter: BirthPresenterProtocol?
    private var setupped = false
    
    private var photo: UIImage?
    private var birthdayDatePicker = UIDatePicker()
    private var birthday: Date?
    
    // MARK: - keyboard stuff -
    private func registerKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(kbWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(kbWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    private func removeKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @objc private func kbWillShow(_ notification: Notification) {
        let userInfo = notification.userInfo
        let kbFrameSize = (userInfo?[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        birthScrollView.setContentOffset(CGPoint(x: 0, y: kbFrameSize.height), animated: true)
    }
    
    @objc private func kbWillHide() {
        birthScrollView.setContentOffset(CGPoint.zero, animated: true)
    }
    
    // MARK: - Birthday field -
    private func setupBirthdayField() {
        var dateToAdd  = DateComponents()
        dateToAdd.year = -maximumAge
        
        let minDate = Calendar.current.date(byAdding: dateToAdd, to: Date())
        
        birthdayDatePicker.datePickerMode = .date
        birthdayDatePicker.maximumDate    = Date()
        birthdayDatePicker.minimumDate    = minDate
        birthdayTextField.inputView       = birthdayDatePicker
        birthdayDatePicker.addTarget(self, action: #selector(dateChanged(datePicker:)), for: .valueChanged)
    }
    
    @objc private func dateChanged(datePicker: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        birthdayTextField.text = dateFormatter.string(from: datePicker.date)
        birthday = datePicker.date
    }
    
    private func checkIntegrity() {
        assert(self.isSetupped, "Birth view controller should be setupped")
        assert(presenter?.isSetupped ?? false, "Birth presenter should be setupped")
    }
    
    private func composePerson() -> Person {
        let newPersone = Person()
        
        newPersone.firstName = firstNameTextField.text ?? ""
        newPersone.lastName  = lastNameTextField .text ?? ""
        newPersone.city      = cityTextField     .text ?? ""
        newPersone.biography = biographyTextField.text ?? ""
        newPersone.gender    = genderSegmentControl.selectedSegmentIndex == 0 ? .male : .female
        newPersone.photo     = photo
        newPersone.birthday  = birthday
        
        return newPersone
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        checkIntegrity()
        setupBirthdayField()
        hideKeyboardWhenTappedAround()
        registerKeyboardNotifications()
    }
    
    deinit {
        removeKeyboardNotifications()
    }
    
    // MARK: - outlets -
    @IBOutlet weak var userPhoto: UIImageView!
    @IBOutlet weak var birthScrollView: UIScrollView!
    
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    @IBOutlet weak var genderSegmentControl: UISegmentedControl!
    
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField : UITextField!
    @IBOutlet weak var cityTextField     : UITextField!
    @IBOutlet weak var birthdayTextField : UITextField!
    
    @IBOutlet weak var biographyTextField: UITextView!
    
    // MARK: - actions -
    @IBAction func takePhotoButtonWasTapped(_ sender: UIButton) {
        getPhoto(source: .camera)
    }
    @IBAction func libraryPhotoWasTapped(_ sender: UIButton) {
        getPhoto(source: .photoLibrary)
    }
    
    
    @IBAction func saveButtonWasTapped(_ sender: UIBarButtonItem) {
        dismissKeyboard()
        let person = composePerson()
        presenter?.registerPerson(person)
    }
    
    @IBAction func firstNameEnterWasTapped(_ sender: UITextField) {
        if !(firstNameTextField.text?.isEmpty ?? true) {
            presenter?.firstNameWasEntered()
        }
    }
    
    @IBAction func lastNameEnterWasTapped(_ sender: UITextField) {
        if !(lastNameTextField.text?.isEmpty ?? true) {
            presenter?.lastNameWasEntered()
        }
    }
    
    @IBAction func cityEnterWasTapped(_ sender: UITextField) {
        if !(cityTextField.text?.isEmpty ?? true) {
            presenter?.cityWasEntered()
        }
    }
    
    @IBAction func genderWasChanged(_ sender: UISegmentedControl) {
        if photo == nil {
            let newGender = sender.selectedSegmentIndex == 0 ? Gender.male : .female
            presenter?.changeGenderTo(newGender)
        }
    }
}

extension BirthViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func getPhoto(source: UIImagePickerControllerSourceType) {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate      = self
            imagePicker.sourceType    = source
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            userPhoto.image = pickedImage
            photo           = pickedImage
        }
        picker.dismiss(animated: true, completion: nil)
    }
}

extension BirthViewController: BirthViewControllerProtocol {
    var isSetupped: Bool {
        return setupped
    }
    
    func setup(presenter: BirthPresenterProtocol) {
        self.presenter = presenter
        self.setupped  = true
    }
    
    func hideKeyboard() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(BirthViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func setSaveButtonStatus(enabled: Bool) {
        saveButton.isEnabled = enabled
    }
    
    func resetDanger() {
        firstNameTextField.setVisualMode(.normal)
        lastNameTextField .setVisualMode(.normal)
        cityTextField     .setVisualMode(.normal)
        birthdayTextField .setVisualMode(.normal)
    }
    
    func makeFocusOnField(_ index: Int, visualMode: TextFieldVisualMode) {
        switch index {
        case 0:
            firstNameTextField.becomeFirstResponder()
            firstNameTextField.setVisualMode(visualMode)
        case 1:
            lastNameTextField.becomeFirstResponder()
            lastNameTextField.setVisualMode(visualMode)
        case 2:
            cityTextField.becomeFirstResponder()
            cityTextField.setVisualMode(visualMode)
        case 3:
            birthdayTextField.becomeFirstResponder()
            birthdayTextField.setVisualMode(visualMode)
        default:
            return
        }
    }
    
    func setPhotoForGender(_ gender: Gender) {
        let imageName   = gender == .male ? maleAvatarName : femaleAvatarName
        userPhoto.image = UIImage(named: imageName)
    }
    
    func setCityIfEmpty(_ city: String) {
        if cityTextField.text == nil || cityTextField.text!.isEmpty {
            cityTextField.text = city
        }
    }
    
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: gotItAlertButton, style: .default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showLocationAskingAlert() {
        let alert = UIAlertController(title: askLocationAlertTitle, message: askLocationAlertMessage, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: gotItAlertButton, style: .cancel, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: openSettionsAlertButton, style: .default, handler: { (action) in
            if let url = URL(string: UIApplicationOpenSettingsURLString) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }))
        self.present(alert, animated: true, completion: nil)
    }
}

extension BirthViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(BirthViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
}
