//
//  BirthPresenterProtocol.swift
//  Community
//
//  Created by Marty on 07/09/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

protocol BirthPresenterProtocol: NSObjectProtocol {
    var isSetupped: Bool {get}
    func setup(view: BirthViewControllerProtocol, router: BirthRouterProtocol, interactor: BirthInteractorProtocol)
    func registerPerson(_ person: Person)
    
    func firstNameWasEntered()
    func lastNameWasEntered()
    func cityWasEntered()
    func birthdayWasEntered()
    
    func changeGenderTo(_ gender: Gender)
    
    func uploadingSucceed()
    func uploadingFailured()
    
    func cityWasFounded(_ city: String)
    func locationWasDenied()
}
