//
//  BirthPresenter.swift
//  Community
//
//  Created by Marty on 07/09/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

class BirthPresenter: NSObject {
    private weak var view : BirthViewControllerProtocol?
    private var router    : BirthRouterProtocol?
    private var interactor: BirthInteractorProtocol?
    private var setupped  = false
}

extension BirthPresenter: BirthPresenterProtocol {
    var isSetupped: Bool {
        return setupped
    }
    
    func setup(view: BirthViewControllerProtocol, router: BirthRouterProtocol, interactor: BirthInteractorProtocol) {
        guard !setupped else {
            return
        }
        
        self.view       = view
        self.router     = router
        self.interactor = interactor
        self.setupped   = true
    }
    
    func registerPerson(_ person: Person) {
        view?.resetDanger()
        
        if person.firstName.isEmpty {
            view?.makeFocusOnField(0, visualMode: .danger)
            return
        } else if person.lastName.isEmpty {
            view?.makeFocusOnField(1, visualMode: .danger)
            return
        } else if person.city.isEmpty {
            view?.makeFocusOnField(2, visualMode: .danger)
            return
        } else if person.birthday == nil {
            view?.makeFocusOnField(3, visualMode: .danger)
            return
        }
        view?.setSaveButtonStatus(enabled: false)
        
        interactor?.registerPerson(person)
    }
    
    func firstNameWasEntered() {
        view?.makeFocusOnField(1, visualMode: .normal)
    }
    
    func lastNameWasEntered() {
        view?.makeFocusOnField(2, visualMode: .normal)
    }
    
    func cityWasEntered() {
        view?.makeFocusOnField(3, visualMode: .normal)
    }
    
    func birthdayWasEntered() {
        view?.dismissKeyboard()
    }
    
    func changeGenderTo(_ gender: Gender) {
        view?.setPhotoForGender(gender)
    }
    
    func uploadingSucceed() {
        router?.dismissModule()
    }
    
    func uploadingFailured() {
        view?.setSaveButtonStatus(enabled: true)
        view?.showAlert(title: "Saving failured", message: "Something was wrong")
    }
    
    func cityWasFounded(_ city: String) {
        view?.setCityIfEmpty(city)
    }
    
    func locationWasDenied() {
        view?.showLocationAskingAlert()
    }
}
