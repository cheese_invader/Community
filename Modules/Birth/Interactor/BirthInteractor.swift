//
//  BirthInteractor.swift
//  Community
//
//  Created by Marty on 07/09/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation
import CoreLocation

private let alreadyAskedKey = "alreadyAsked"

class BirthInteractor: NSObject {
    private weak var presenter: BirthPresenterProtocol?
    private let networkWorker   = NetworkWorker.shared
    private let locationManager = CLLocationManager()
    
    init(presenter: BirthPresenterProtocol) {
        super.init()
        self.presenter = presenter
        setupLocationManager()
    }
    
    private func addPersonToDataBase(_ person: Person) {
        let context = CoreDataStackManager.shared.viewContext
        context.perform {
            let newPerson = CDPerson(context: context)
            newPerson.composeWithEntity(person)
            try? context.save()
        }
    }
}

extension BirthInteractor: BirthInteractorProtocol {
    func registerPerson(_ person: Person) {
        networkWorker.uploadPerson(person, completionSuccess: { [weak self] (person) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.addPersonToDataBase(person)
            DispatchQueue.main.async {
                strongSelf.presenter?.uploadingSucceed()
            }
        }, completionFailure: { [weak self] in
            guard let strongSelf = self else {
                return
            }
            DispatchQueue.main.async {
                strongSelf.presenter?.uploadingFailured()
            }
        })
    }
}

extension BirthInteractor: CLLocationManagerDelegate {
    private func setupLocationManager() {
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
    }
    
    private func stopLocationManager() {
        locationManager.stopUpdatingLocation()
        locationManager.delegate = nil
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            let geocoder = CLGeocoder()
            geocoder.reverseGeocodeLocation(location) { [weak self] (placemarks, error) in
                guard let strongSelf = self else {
                    return
                }
                
                guard error == nil else {
                    strongSelf.stopLocationManager()
                    return
                }
                
                if let placemarks = placemarks, let placemark = placemarks.first {
                    let city = placemark.locality!
                    strongSelf.presenter?.cityWasFounded(city)
                    strongSelf.stopLocationManager()
                }
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .denied {
            let alreadyAsked = UserDefaults.standard.bool(forKey: alreadyAskedKey)
            if !alreadyAsked {
                presenter?.locationWasDenied()
                UserDefaults.standard.set(true, forKey: alreadyAskedKey)
            }
        }
    }
}
