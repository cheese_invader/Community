//
//  BirthInteractorProtocol.swift
//  Community
//
//  Created by Marty on 07/09/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

protocol BirthInteractorProtocol: NSObjectProtocol {
    func registerPerson(_ person: Person)
}
