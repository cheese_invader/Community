//
//  BirthRouter.swift
//  Community
//
//  Created by Marty on 07/09/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import UIKit

private let storyboardName = "Birth"
private let storyboardID   = "BirthStoryboard"

class BirthRouter: NSObject {
    private weak var presenter: BirthPresenterProtocol?
    private weak var navigationController: NavigationViewController?
    
    init(presenter: BirthPresenterProtocol, navigationController: NavigationViewController) {
        self.navigationController = navigationController
        self.presenter            = presenter
    }
}

extension BirthRouter: BirthRouterProtocol {
    static func assembleModule(embeddedIn navigationController: NavigationViewController) -> BirthViewController {
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let birthVC    = storyboard.instantiateViewController(withIdentifier: storyboardID) as! BirthViewController
        
        let presenter  = BirthPresenter()
        let interactor = BirthInteractor(presenter: presenter)
        let router     = BirthRouter(presenter: presenter, navigationController: navigationController)
        
        presenter.setup(view: birthVC, router: router, interactor: interactor)
        birthVC  .setup(presenter: presenter)
        
        return birthVC
    }
    
    func dismissModule() {
        navigationController?.popViewController(animated: true)
    }
}
