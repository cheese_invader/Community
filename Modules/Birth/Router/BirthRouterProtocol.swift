//
//  BirthRouterProtocol.swift
//  Community
//
//  Created by Marty on 07/09/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation
import UIKit

protocol BirthRouterProtocol: NSObjectProtocol {
    static func assembleModule(embeddedIn navigationController: NavigationViewController) -> BirthViewController
    func dismissModule()
}
