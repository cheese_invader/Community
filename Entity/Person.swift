//
//  Person.swift
//  Community
//
//  Created by Marty on 09/09/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation
import UIKit

class Person {
    var id        = -1
    var firstName = ""
    var lastName  = ""
    var biography = ""
    var city      = ""
    var gender    = Gender.male
    var photo     : UIImage?
    var birthday  : Date?
    
    static func makeFromCDEntity(_ entity: CDPerson) -> Person {
        let person = Person()
        
        person.id = Int(entity.identifier)
        person.firstName = entity.firstName ?? ""
        person.lastName  = entity.lastName  ?? ""
        person.city      = entity.city      ?? ""
        person.biography = entity.biography ?? ""
        person.gender    = entity.isMale ? .male : .female
        person.birthday  = entity.birthday
        
        if let photoBinary = entity.photo {
            person.photo = UIImage(data: photoBinary)
        }
        
        return person
    }
}
