//
//  Gender.swift
//  Community
//
//  Created by Marty on 09/09/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

enum Gender {
    case male
    case female
}
