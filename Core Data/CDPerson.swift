//
//  CDPerson.swift
//  Community
//
//  Created by Marty on 11/09/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import CoreData
import UIKit

class CDPerson: NSManagedObject {
    func composeWithEntity(_ entity: Person) {
        self.identifier = Int64(entity.id)
        self.firstName  = entity.firstName
        self.lastName   = entity.lastName
        self.city       = entity.city
        self.biography  = entity.biography
        self.isMale     = entity.gender == .male
        self.birthday   = entity.birthday
        if let userPhoto = entity.photo {
            // TODO: - Waiting for Swift 4.2 (pngData()) -
            // https://developer.apple.com/documentation/uikit/uiimage/1624096-pngdata
            self.photo = UIImageJPEGRepresentation(userPhoto, 1.0)
        }
    }
}
